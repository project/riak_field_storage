The Riak Field Storage module stores fields in a Riak database.

Installation:
- Put this module in the proper place
- In the module folder, run `git clone https://github.com/basho/riak-php-client.git`
- In settings.php, you can configure the Riak connection -- it defaults to:
  $conf['riak_connection'] = array('host' => 'localhost', 'port' => '8098');
- You can also configure in settings.php the default field storage engine. To
  set it to Riak, add the following line:
  $conf['field_storage_default'] = 'riak_field_storage';


Note:
We reimplement MapReduce because Riak's MapReduce throws an error :-).